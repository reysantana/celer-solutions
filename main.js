let $ = require('jquery');
let ipc = require('electron').ipcMain;
let ipcRenderer = require('electron').ipcRenderer;
const path = require('path');
const {getAppDataPath} = require("appdata-path");
var knex = require("knex")({
	client: "sqlite3",
	connection: {
        filename: path.join(getAppDataPath('celer-solutions'), 'database.db')
    }, 
    useNullAsDefault: true
});
const async = require('async');
const { dialog } = require('electron').remote;
const remote = require('electron').remote;
const app = remote.app;
const fs = require('fs');
const axios = require('axios');
const qs = require('qs');

let pushNotification = 0;
let audioNotification = 0;

$('#proxy-div').hide();
$('#setting-div').hide();
$('#dialogs-div').hide();
$('.create-task-dialog').hide();
$('.mass-edit-dialog').hide();
$('.task-edit-dialog').hide();
$('.add-proxies-dialog').hide();

ipcRenderer.on('mainWindowLoaded', function() {
    $('.lds-dual-ring').hide();
});

function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
function LetterAvatar (name, size) {

    name  = name || '';
    size  = size || 60;

    var colours = [
            "#1abc9c", "#2ecc71", "#3498db", "#9b59b6", "#34495e", "#16a085", "#27ae60", "#2980b9", "#8e44ad", "#2c3e50", 
            "#f1c40f", "#e67e22", "#e74c3c", "#ecf0f1", "#95a5a6", "#f39c12", "#d35400", "#c0392b", "#bdc3c7", "#7f8c8d"
        ],

        nameSplit = String(name).toUpperCase().split(' '),
        initials, charIndex, colourIndex, canvas, context, dataURI;


    if (nameSplit.length == 1) {
        initials = nameSplit[0] ? nameSplit[0].charAt(0):'?';
    } else {
        initials = nameSplit[0].charAt(0) + nameSplit[1].charAt(0);
    }

    if (window.devicePixelRatio) {
        size = (size * window.devicePixelRatio);
    }
        
    charIndex     = (initials == '?' ? 72 : initials.charCodeAt(0)) - 64;
    colourIndex   = charIndex % 20;
    canvas        = document.createElement('canvas');
    canvas.width  = size;
    canvas.height = size;
    context       = canvas.getContext("2d");
        
    context.fillStyle = colours[colourIndex - 1];
    context.fillRect (0, 0, canvas.width, canvas.height);
    context.font = Math.round(canvas.width/2)+"px Arial";
    context.textAlign = "center";
    context.fillStyle = "#FFF";
    context.fillText(initials, size / 2, size / 1.5);

    dataURI = canvas.toDataURL();
    canvas  = null;

    return dataURI;
}

LetterAvatar.transform = function() {

    Array.prototype.forEach.call(document.querySelectorAll('img[avatar]'), function(img, name) {
        name = img.getAttribute('avatar');
        img.src = LetterAvatar(name, img.getAttribute('width'));
        img.removeAttribute('avatar');
        img.setAttribute('alt', name);
    });
};


// AMD support
if (typeof define === 'function' && define.amd) {
    
    define(function () { return LetterAvatar; });

// CommonJS and Node.js module support.
} else if (typeof exports !== 'undefined') {
    
    // Support Node.js specific `module.exports` (which can be a function)
    if (typeof module != 'undefined' && module.exports) {
        exports = module.exports = LetterAvatar;
    }

    // But always support CommonJS module 1.1.1 spec (`exports` cannot be a function)
    exports.LetterAvatar = LetterAvatar;

} else {
    
    window.LetterAvatar = LetterAvatar;

    // document.addEventListener('DOMContentLoaded', function(event) {
    //     LetterAvatar.transform();
    // });
}

$(document).on('click', '.tab-item', function() {
    
    $('.tab-item').removeClass('active');
    $(this).addClass('active');
    
    $('#task-div').hide();
    $('#proxy-div').hide();
    $('#setting-div').hide();
    
    switch ($(this).prop('id')) {
        case 'btn-task':
            $('#task-div').show();
            break;
        case 'btn-proxy':
            $('#proxy-div').show();
            break;
        case 'btn-setting':
            $('#setting-div').show();
            break;
    }
});

$(document).on('click', '.create_tasks', function() {
    $('#dialogs-div').show();
    $('.create-task-dialog').show();
    $('.create-task-dialog').css('opacity', 1);
});

$(document).on('click', '.create-task-buttons .create', function() {
    var url = $('#new_url').val();
    var count = $('#task_cnt').val();
    var sel_value = $('#task_proxy').val();
    if(url != '' && count >= 1 && $('#task_proxy').val() != ''){
        knex('proxy_list').where('status', '>=', 0).then(function(result_array){
            for(var i = 0 ; i < count ; i++){
                var result;
                if(sel_value == "0"){
                    result = result_array[getRandomInt(result_array.length)];
                    knex('task_list').insert({url : url, status : 0, proxy_id : result['id']}).then(function(res){     
                    });
                }
                else if(sel_value == "1"){
                    knex('task_list').insert({url : url, status : 0, proxy_id : 1}).then(function(res){            
                    });
                }
                else{
                    knex('task_list').insert({url : url, status : 0, proxy_id : sel_value}).then(function(res){
                        
                    });
                }
                
                
            }
            refresh_task_table();
        });
    }
    $('#new_url').val('');
    $('#task_cnt').val('');
    $('#dialogs-div').hide();
    $('.create-task-dialog').hide();
    $('.create-task-dialog').css('opacity', 0);
});

$(document).on('click', '.create-task-buttons .cancel', function() {
    $('#dialogs-div').hide();
    $('.create-task-dialog').hide();
    $('.create-task-dialog').css('opacity', 0);
});

$(document).on('click', '.mass_edit', function() {
    $('#task_url').val('');
    $('#dialogs-div').show();
    $('.mass-edit-dialog').show();
    $('.mass-edit-dialog').css('opacity', 1);
});

$(document).on('click', '.mass-edit-buttons .confirm', function() {
    var task_url = $('#task_url').val();
    for (let i = 0 ; i < $('#tbl_task tr').length ; i ++){
        let task_id = $('#tbl_task tr').eq(i + 1).data('value');
        let tr_no = getRandomInt($('#tbl_proxy tr').length - 1);
        let proxy_id = $('#tbl_proxy tr').eq(tr_no + 1).data('value');
        knex('task_list').where('id', task_id).update({
            proxy_id : proxy_id,
            url : task_url            
        }).then(function(){});  
    }
    refresh_task_table();
    ipcRenderer.send('mass_refresh', task_url);
    $('#dialogs-div').hide();
    $('.mass-edit-dialog').hide();
    $('.mass-edit-dialog').css('opacity', 0);
});

$(document).on('click', '.mass-edit-buttons .cancel', function() {
    $('#dialogs-div').hide();
    $('.mass-edit-dialog').hide();
    $('.mass-edit-dialog').css('opacity', 0);
});

$(document).on('click', '.add_proxies', function() {
    $('#dialogs-div').show();
    $('.add-proxies-dialog').show();
    $('.add-proxies-dialog').css('opacity', 1);
});

$(document).on('click', '.remove_proxies', function() {
    knex('proxy_list').where('id', '>', 1).delete().then(function(){
        load_data(false);
    });
});


$(document).on('click', '.add-proxies-buttons .confirm', function() {
    let lines = $('#proxy_list').val().split('\n');

    for (let i = 0 ; i < lines.length; i ++){
        let data = lines[i].split(':');
        if (data[0] != null // proxy IP
            && data[1] != null // port
            && data[2] != null // username
            && data[3] != null) // password 
        {
            let randID = "proxy_status_" + generateRand(8);
            knex('proxy_list').insert({proxy : data[0], port : data[1], user : data[2], pass : data[3], status : 0}).then(function(result) {
                html = '<tr id = "proxy-' + result[0] + '" data-value="' + result[0] + '" data-ip="' + data[0] + '" data-port="' + data[1] + '" data-user="' + data[2] + '" data-pass="' + data[3] +'"> \
                    <td>#' + ('000' + result[0]).substr(-3) + '</td> \
                    <td><div class="url">' + data[0] +  ' (' + data[1] + ')</div></td> \
                    <td><div id="' + randID + '"class="status queued">Checking</div></td> \
                    <td> \
                        <div class="action-buttons"> \
                            <span class="proxy-btn play"></span> \
                            <span class="proxy-btn pause"></span> \
                            <span class="proxy-btn delete"></span> \
                        </div> \
                    </td> \
                </tr>';
                $('#tbl_proxy').append(html);

                $.ajax({
                    "async": true,
                    "crossDomain": true,
                    "url": "https://gogetproxy.com/checker/check",
                    "method": "POST",
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                    "data": {
                        "proxies[]": data[0] + ":" + data[1] + ":" + data[2] + ":" + data[3]
                    }
                })
                .done(function (response) {
                    respJson = JSON.parse(response);
                    if (respJson.https == true || respJson.socks4 == true || respJson.socks5 == true) {
                        knex('proxy_list').where({ id: result[0]}).update({ status: 1 }).then(function(res){ 
                            $('#' + randID).removeClass('queued');
                            $('#' + randID).addClass('working');
                            $('#' + randID).text("Working");
                        });
                    } else {
                        knex('proxy_list').where({ id: result[0]}).update({ status: 2 }).then(function(res){
                            $('#' + randID).removeClass('queued');
                            $('#' + randID).addClass('error');
                            $('#' + randID).text("Error");
                        });
                    }
                });
            });
        }
    }
    add_proxy_select();
    $('#proxy_list').val('');
    $('#dialogs-div').hide();
    $('.add-proxies-dialog').hide();
    $('.add-proxies-dialog').css('opacity', 0);
});

$(document).on('click', '.add-proxies-buttons .cancel', function() {
    $('#dialogs-div').hide();
    $('.add-proxies-dialog').hide();
    $('.add-proxies-dialog').css('opacity', 0);
});


$(document).on('click', '.export_proxies', function() {

    knex('proxy_list')
    .select('proxy', 'port', 'user', 'pass')
    .where('status', '>', -1)
    .then(function(data) {
        let content = '';
        for (let i = 0; i < data.length; i ++) {
            content = content + data[i].proxy + ":" + data[i].port + ":" + data[i].user + ":" + data[i].pass + "\n";
        }

        dialog.showSaveDialog({
            title: 'Export Proxy',
            filters: [{ name: 'PROXY file', extensions: ['proxy'] }],
            defaultPath: app.getPath('documents')
        }, function (file) {
            
            fs.writeFile(file, content, function (err) {
                if (err) {
                    return;
                }
              });
        });
    });
});

$(document).on('click', '.import_proxies', function() {
    
    dialog.showOpenDialog({
        title: 'Import Proxy',
        filters: [{ name: 'PROXY file', extensions: ['proxy'] }],
        properties: ['openFile'], 
        defaultPath: app.getPath('documents')
    }, function (files) {
        if (files !== undefined) {
            fs.readFile(files[0], 'utf-8', (err, data) => {
                if (err) {
                    return;
                }
        
                let lines = data.split('\n');

                for (let i = 0 ; i < lines.length; i ++) {
                    
                    let data = lines[i].split(':');

                    if (data[0] != null && data[1] != null && data[2] != null && data[3] != null) {
                        let randID = "proxy_status_" + generateRand(8);
                        knex('proxy_list').insert({proxy : data[0], port : data[1], user : data[2], pass : data[3], status : 0}).then(function(result) {
                            html = '<tr id = "proxy-' + result['id'] + '" data-value="' + result['id'] + '" data-ip="' + result['proxy'] + '" data-port="' + result['port'] + '" data-user="' + result['user'] + '" data-pass="' + result['pass'] +'"> \
                                <td>#' + ('000' + result[0]).substr(-3) + '</td> \
                                <td><div class="url">' + data[0] +  ' (' + data[1] + ')</div></td> \
                                <td><div id="' + randID + '"class="status queued">Checking</div></td> \
                                <td> \
                                    <div class="action-buttons"> \
                                        <span class="proxy-btn play"></span> \
                                        <span class="proxy-btn pause"></span> \
                                        <span class="proxy-btn delete"></span> \
                                    </div> \
                                </td> \
                            </tr>';
                            $('#tbl_proxy').append(html);
        
                            $.ajax({
                                "async": true,
                                "crossDomain": true,
                                "url": "https://gogetproxy.com/checker/check",
                                "method": "POST",
                                "headers": {
                                    "Content-Type": "application/x-www-form-urlencoded",
                                },
                                "data": {
                                    "proxies[]": data[0] + ":" + data[1] + ":" + data[2] + ":" + data[3]
                                }
                            })
                            .done(function (response) {
                                respJson = JSON.parse(response);
                                if (respJson.https == true || respJson.socks4 == true || respJson.socks5 == true) {
                                    knex('proxy_list').where({ id: result[0]}).update({ status: 1 }).then(function(res){ 
                                        $('#' + randID).removeClass('queued');
                                        $('#' + randID).addClass('working');
                                        $('#' + randID).text("Working");
                                    });
                                } else {
                                    knex('proxy_list').where({ id: result[0]}).update({ status: 2 }).then(function(res){
                                        $('#' + randID).removeClass('queued');
                                        $('#' + randID).addClass('error');
                                        $('#' + randID).text("Error");
                                    });
                                }
                            });
                        });
                    }
                }
                add_proxy_select();
            });
        }
    });
});

$(document).on('click', '.auth-buttons .close', function() {
    ipcRenderer.send('close_auth');
});

ipcRenderer.on('taskClosed', function(event, title) {
    id = title.substring(4);
    knex('task_list').where('id', id).update({
        status : 0
    }).then(function(){
        refresh_task_table();
    });
});

ipcRenderer.on('taskUrlChanged', function(event, title) {
    id = title.substring(4);
    knex('task_list').where('id', id).update({
        status : 1
    }).then(function() {
        // ipcRenderer.send('taskUrlChanged', title);
        refresh_task_table();
    });
});

$(document).on('click', '.auth-buttons .activate', function() {
    let license = $('.license-input').val();

    if ($('.license-input').val().length < 19) {
        $('.license-field .message').text("Input a valid license.");
        $('.license-input').val('');
        $('.license-input').focus();
    }
    else {
        $.ajax({
            type: "POST",
            url: "https://celerqueueserver.com/index.php/API/login",
            data: {
                license: $('.license-input').val()
            },
            success: function(data, status, xhr) {
                $('.license-field .message').text(data.message);
                if (data.name.length > 0 && data.email.length > 0 && data.message == "Success") {
                    $('.license-field .message').css('color', '#78FF5E');
                    ipcRenderer.send('auth_success', license, data.name, data.email, pushNotification, audioNotification);
                }
                else {
                    $('.license-input').val('');
                    $('.license-input').focus();
                }
            },
            error: function(xhr, status, err) {
                $('.license-field .message').text(err);
                $('.license-input').val('');
                $('.license-input').focus();
            }, 
            dataType: 'json'
        });
    }
});

$(document).on('click','.task-edit-buttons .cancel',function() {
    $('#dialogs-div').hide();
    $('.task-edit-dialog').hide();
    $('.task-edit-dialog').css('opacity', 0);
});

$(document).on('click','.task-edit-buttons-top .show',function() {
    var task_id = $(this).parent().parent().find('#edit_url').attr('data-value');
    var title = "Task" + task_id;
    ipcRenderer.send('maximizeTask', title);
});

$(document).on('click','.task-edit-buttons-top .refresh',function() {
    var task_id = $(this).parent().parent().find('#edit_url').attr('data-value');
    var title = "Task" + task_id;
    ipcRenderer.send('refreshTask', title);
});

$(document).on('click','.task-edit-buttons-top .queue',function() {
    var task_id = $(this).parent().parent().find('#edit_url').attr('data-value');
    var title = "Task" + task_id;
    knex('task_list').where('id', task_id).update({
        status : 2
    }).then(function(){
        // ipcRenderer.send('queueTask', "Task" + task_id, res['url'], res['host'], res['port'], res['user'], res['pass']);
        refresh_task_table();
    });
});

$(document).on('click', '.task-edit-buttons .confirm', function() {
    var proxy_id = $('#custom_proxy_sel').val();
    var url = $('#edit_url').val();
    var task_id = $('#edit_url').attr('data-value');
    if(proxy_id == 0){
        tr_no = getRandomInt($('#tbl_proxy tr').length - 1);
        proxy_id = $('#tbl_proxy tr').eq(tr_no + 1).data('value');
    }
    
    knex('task_list').where('id', task_id).update({
        proxy_id : proxy_id,
        url : url
    }).then(function() {
        ipcRenderer.send('closeTask', "Task" + task_id);
        refresh_task_table();
    });
    $('#dialogs-div').hide();
    $('.task-edit-dialog').hide();
    $('.task-edit-dialog').css('opacity', 0);
});

$(document).on('click', '.task-btn.play', function() {
    let parentDiv = $(this).parent().parent().parent();
    ipcRenderer.send('runTask', "Task" + parentDiv.data('value'), parentDiv.find('.url').text(), 
    parentDiv.data('ip'), parentDiv.data('port'), parentDiv.data('user'), parentDiv.data('pass'));
    knex('task_list').where('id', parentDiv.data('value')).update({
        status : 2
    }).then(function(){
        refresh_task_table();
    });
});

$(document).on('input', '#task_cnt', function(){
    let count = $(this).val();
    if(count >= 2){
        $('#task_proxy').val(0);
        $('#task_proxy').css('color', '#A9A9A9');
        $('#task_proxy').prop('disabled', 'disabled');
    }
    else{
        $('#task_proxy').val(1);
        $('#task_proxy').prop('disabled', false);
        $('#task_proxy').css('color', '');
    }
});

$(document).on('click', '.btn.close_all', function(){
    ipcRenderer.send('minimizeTask');
});

$(document).on('click', '.btn.open_all', function(){
    ipcRenderer.send('maximizeTask');
});

$(document).on('click', '.task-btn.edit', function() {
    let parentDiv = $(this).parent().parent().parent();
    var url = $(parentDiv).find('.url').html();
    var proxy_id = $(parentDiv).data('proxy');
    var task_id = $(parentDiv).data('value');
    $('#custom_proxy_sel').val(proxy_id);
    $('#edit_url').val(url);
    $('#edit_url').attr('data-value', task_id);
    $('#dialogs-div').show();
    $('.task-edit-dialog').show();
    $('.task-edit-dialog').css('opacity', 1);
});

$(document).on('click', '.task-btn.delete', function() {
    let parentDiv = $(this).parent().parent().parent();
    knex('task_list').where('id',  parentDiv.data('value')).del().then(function(res){      
        ipcRenderer.send('closeTask', "Task" + parentDiv.data('value'));
        refresh_task_table();
    });
});

$(document).on('click', '.proxy-btn.play', function() {
    let parentDiv = $(this).parent().parent().parent();
    let statusDiv = $(parentDiv).find('.status');
    let url = $(parentDiv).find('.url').text().split(' (');
    $(statusDiv).removeAttr('class');
    $(statusDiv).addClass('status');
    $(statusDiv).addClass('queued');
    $(statusDiv).text('Checking');

    $.ajax({
        "async": true,
        "crossDomain": true,
        "url": "https://gogetproxy.com/checker/check",
        "method": "POST",
        "headers": {
            "Content-Type": "application/x-www-form-urlencoded",
        },
        "data": {
            "proxies[]": parentDiv.data('ip') + ":" + parentDiv.data('port') + ":" + parentDiv.data('user') + ":" + parentDiv.data('pass')
        }
    })
    .done(function (response) {
        respJson = JSON.parse(response);
        if (respJson.https == true || respJson.socks4 == true || respJson.socks5 == true) {
            knex('proxy_list').where({ id: parentDiv.data('value')}).update({ status: 1 }).then(function(res){ 
                $(statusDiv).removeClass('queued');
                $(statusDiv).addClass('working');
                $(statusDiv).text("Working");
                refresh_task_table();
            });
        } else {
            knex('proxy_list').where({ id: parentDiv.data('value')}).update({ status: 2 }).then(function(res){
                $(statusDiv).removeClass('queued');
                $(statusDiv).addClass('error');
                $(statusDiv).text("Error");
                refresh_task_table();
            });
        }
    });
});

$(document).on('click', '.proxy-btn.pause', function() {
    let parentDiv = $(this).parent().parent().parent();
    knex('proxy_list').where('id', parentDiv.data('value')).andWhere('status', '>=', 0).update({
        status : 3
    }).then(function() {
        let statusDiv = $(parentDiv).find('.status');
        $(statusDiv).removeAttr('class');
        $(statusDiv).addClass('status');
        $(statusDiv).addClass('error');
        $(statusDiv).text('Stopped');
        refresh_task_table();
    });
});

$(document).on('click', '.btn.start_tasks', function() {
    knex('task_list')
    .join('proxy_list', 'task_list.proxy_id', 'proxy_list.id')
    .select(
        'task_list.id as id', 
        'task_list.url as url', 
        'task_list.status as status', 
        'proxy_list.status as proxy_status', 
        'proxy_list.proxy as host', 
        'proxy_list.port as port', 
        'proxy_list.id as proxy_id', 
        'proxy_list.user as user', 
        'proxy_list.pass as pass')
    .where('task_list.status', 0)
    .then(function(result) {
        for( let i = 0; i < result.length; i ++ ) {
            res = result[i];
            ipcRenderer.send('runTask', "Task" + res['id'], res['url'], res['host'], res['port'], res['user'], res['pass']);
        }
    });
    knex('task_list').update({
        status : 2
    }).then(function(){
        refresh_task_table();
    });
});

$(document).on('click', '.task-btn.pause', function() {
    let parentDiv = $(this).parent().parent().parent();
    knex('task_list').where('id', parentDiv.data('value')).update({
        status : 0
    }).then(function(){
        ipcRenderer.send('closeTask', "Task" + parentDiv.data('value'));
        refresh_task_table();
    });
});


$(document).on('click', '.btn.stop_tasks', function() {
    knex('task_list').where('status', '!=', 0).update({
        status : 0
    }).then(function(){
        ipcRenderer.send('closeAllTasks');
        refresh_task_table();
    });
});

$(document).on('click', '.btn.queue_all', function() {
    let resIndex = 0;
    knex('task_list')
    .join('proxy_list', 'task_list.proxy_id', 'proxy_list.id')
    .select(
        'task_list.id as id', 
        'task_list.url as url', 
        'task_list.status as status', 
        'proxy_list.status as proxy_status', 
        'proxy_list.proxy as host', 
        'proxy_list.port as port', 
        'proxy_list.id as proxy_id', 
        'proxy_list.user as user', 
        'proxy_list.pass as pass')
    .where('task_list.status', 1)
    .then(function(result) {
        for( let i = 0; i < result.length; i ++ ) {
            res = result[i];
            knex('task_list').where('id', res['id']).update({
                status : 2
            }).then(function(){
                resIndex ++;
                // ipcRenderer.send('queueTask', "Task" + res['id'], res['url'], res['host'], res['port'], res['user'], res['pass']);
                if (resIndex == result.length) {
                    refresh_task_table();
                }
            });
        }
    });

});

$(document).on('click', '.btn.delete_tasks', function() {
    knex('task_list').delete().then(function() {
        ipcRenderer.send('closeAllTasks');
        refresh_task_table();
    });
});

$(document).on('click', '.proxy-btn.delete', function() {
    let parentDiv = $(this).parent().parent().parent();
    knex('proxy_list').where('id', parentDiv.data('value')).andWhere('status', '>=', 0).update({
        status : -1
    }).then(function() {
        $(parentDiv).remove();
        load_data(false);
    });
});

$(document).on('click', '.audio-notification input', async function() {
    audioNotification = $(this).is(":checked") == true ? 1 : 0;
    await knex('settings').where({ id: 1 }).update({ audio_notify: audioNotification });
});

$(document).on('click', '.push-notification input', async function() {
    pushNotification = $(this).is(":checked") == true ? 1 : 0;
    await knex('settings').where({ id: 1 }).update({ push_notify: pushNotification });
});

$(document).on('click', '.profile-buttons .btn.logout', function() {
    ipcRenderer.send('logout', pushNotification, audioNotification);
});

$(document).on('click', '.profile-buttons .btn.deactivate', function() {
    ipcRenderer.send('deactivate', pushNotification, audioNotification);
});
function refresh_task_table(){
    $('#tbl_task tr:not(:first)').remove();
    knex('task_list').join('proxy_list', 'task_list.proxy_id', 'proxy_list.id').select('task_list.id as id', 
    'task_list.url as url', 'task_list.status as status', 'proxy_list.status as proxy_status', 
    'proxy_list.proxy as host', 'proxy_list.port as port', 'proxy_list.id as proxy_id', 'proxy_list.user as user', 'proxy_list.pass as pass').then(function(result){
        for(var i = 0; i < result.length; i ++){
            res = result[i];
            let status = (res['proxy_status'] == 1?"on":"off");
            let task_status = '<div class="status error">Stopped</div>';
            if (res['status'] == 1)
                task_status = '<div class="status working">Passed</div>';
            else if(res['status'] == 2)
                task_status = '<div class="status queued">Queued</div>';
            if(res['proxy_id'] == 1){
                html = '<tr data-value="' + res['id'] + '" data-proxy="1" ' + '" data-ip="localhost" data-port="" data-user="" data-pass=""> \
                    <td>#' + ('000' + res['id']).substr(-3) + '</td>\
                    <td><div class="url">' + res['url'] + '</div></td>\
                    <td><div class="proxy on"></div></td>\
                    <td>' + task_status + '</td>\
                    <td>\
                        <div class="action-buttons">\
                            <span class="task-btn play"></span>\
                            <span class="task-btn pause"></span>\
                            <span class="task-btn delete"></span>\
                            <span class="task-btn edit"></span>\
                        </div>\
                    </td>\
                </tr>';
                $('#tbl_task').append(html);  
            }
            else{
                html = '<tr data-value="' + res['id'] + '" data-proxy="' + res['proxy_id'] + '" data-ip="' + res['host'] + '" data-port="' + res['port'] + '" data-user="' + res['user'] + '" data-pass="' + res['pass'] +'"> \
                    <td>#' + ('000' + res['id']).substr(-3) + '</td>\
                    <td><div class="url">' + res['url'] + '</div></td>\
                    <td><div class="proxy '+ status + '"></div></td>\
                    <td>' + task_status + '</td>\
                    <td>\
                        <div class="action-buttons">\
                            <span class="task-btn play"></span>\
                            <span class="task-btn pause"></span>\
                            <span class="task-btn delete"></span>\
                            <span class="task-btn edit"></span>\
                        </div>\
                    </td>\
                </tr>';
                $('#tbl_task').append(html); 
            }
                     
        }
    });
}
function add_proxy_select(){
    $('#task_proxy').empty();
    $('#custom_proxy_sel').empty();
    $('#custom_proxy_sel').append('<option value="1">Local Host</option>');
	$('#custom_proxy_sel').append('<option value="0">Random Proxy</option>');
    $('#task_proxy').append('<option value="1">Local Host</option>');
	$('#task_proxy').append('<option value="0">Random Proxy</option>');
    knex('proxy_list').where('status', '>=', 0).then(function(result) {
        async.map(result, function(item, callback) {
            let exists = 0;
            option = '<option value="' + item['id'] + '">' + item['proxy'] + ':' + item['port'] + '</option>';
            exists = $('#task_proxy option[value="'+ item['id'] +'"]').length;
            if (exists == 0) {
                $('#task_proxy').append(option);
                $('#custom_proxy_sel').append(option);
            }

            callback(null, item);
        }, function(err, results) {
        });
    });
}

function load_data(check_flag) {
    $('#tbl_task tr:not(:first)').remove();
    $('#tbl_proxy tr:not(:first)').remove();
    $('#task_proxy').empty();
    $('#custom_proxy_sel').empty();
    $('#custom_proxy_sel').append('<option value="1">Local Host</option>');
	$('#custom_proxy_sel').append('<option value="0">Random Proxy</option>');
    $('#task_proxy').append('<option value="1">Local Host</option>');
	$('#task_proxy').append('<option value="0">Random Proxy</option>');
    if(check_flag == true){
        knex('task_list').update({
            status : 0
        }).then(function(){});
    }
    knex('task_list').join('proxy_list', 'task_list.proxy_id', 'proxy_list.id').select('task_list.id as id', 
    'task_list.url as url', 'task_list.status as status', 'proxy_list.status as proxy_status', 
    'proxy_list.proxy as host', 'proxy_list.port as port', 'proxy_list.id as proxy_id', 'proxy_list.user as user', 'proxy_list.pass as pass').then(function(result){
        for(var i = 0; i < result.length; i ++){
            res = result[i];
            let status = (res['proxy_status'] == 1?"on":"off");
            let task_status = '<div class="status error">Stopped</div>';
            if (res['status'] == 1)
                task_status = '<div class="status working">Passed</div>';
            else if(res['status'] == 2)
                task_status = '<div class="status queued">Queued</div>';
            if(res['proxy_id'] == 1){
                html = '<tr data-value="' + res['id'] + '" data-proxy="1" ' + ' data-ip="localhost" data-port="" data-user="" data-pass=""> \
                    <td>#' + ('000' + res['id']).substr(-3) + '</td>\
                    <td><div class="url">' + res['url'] + '</div></td>\
                    <td><div class="proxy on"></div></td>\
                    <td>' + task_status + '</td>\
                    <td>\
                        <div class="action-buttons">\
                            <span class="task-btn play"></span>\
                            <span class="task-btn pause"></span>\
                            <span class="task-btn delete"></span>\
                            <span class="task-btn edit"></span>\
                        </div>\
                    </td>\
                </tr>';
                $('#tbl_task').append(html);  
            }
            else{
                html = '<tr data-value="' + res['id'] + '" data-proxy="' + res['proxy_id'] + '" data-ip="' + res['host'] + '" data-port="' + res['port'] + '" data-user="' + res['user'] + '" data-pass="' + res['pass'] +'"> \
                    <td>#' + ('000' + res['id']).substr(-3) + '</td>\
                    <td><div class="url">' + res['url'] + '</div></td>\
                    <td><div class="proxy '+ status + '"></div></td>\
                    <td>' + task_status + '</td>\
                    <td>\
                        <div class="action-buttons">\
                            <span class="task-btn play"></span>\
                            <span class="task-btn pause"></span>\
                            <span class="task-btn delete"></span>\
                            <span class="task-btn edit"></span>\
                        </div>\
                    </td>\
                </tr>';
                $('#tbl_task').append(html); 
            }      
        }
    });
    knex('proxy_list').where('status', '>=', 0).then(function(result) {
        for(var i = 0; i < result.length; i ++){
            let item = result[i];
            let exists = 0;
            let status ="<div id='proxy-" + item['id'] + "' class='status queued'>Checking</div>";
            if(item['status'] == 2)
                status = "<div id='" + item['id'] + "' class='status error'>Error</div>";
            else if(item['status'] == 3)
                status = "<div id='" + item['id'] + "' class='status error'>Stopped</div>";
            else if(item['status'] == 1)
                status = "<div id='" + item['id'] + "' class='status working'>Working</div>";
            html = '<tr id = "proxy-' + item['id'] + '" data-value="' + item['id'] + '" data-ip="' + item['proxy'] + '" data-port="' + item['port'] + '" data-user="' + item['user'] + '" data-pass="' + item['pass'] +'"> \
                <td>#' + ('000' + item['id']).substr(-3) + '</td> \
                <td><div class="url">' + item['proxy'] + ' (' + item['port'] + ')</div></td> \
                <td>' + status + '</td> \
                <td> \
                    <div class="action-buttons"> \
                        <span class="proxy-btn play"></span> \
                        <span class="proxy-btn pause"></span> \
                        <span class="proxy-btn delete"></span> \
                    </div> \
                </td> \
            </tr>';
            $('#tbl_proxy').append(html);
            option = '<option value="' + item['id'] + '">' + item['proxy'] + ':' + item['port'] + '</option>';
            exists = $('#task_proxy option[value="'+ item['id'] +'"]').length;
            if (exists == 0) {
                $('#task_proxy').append(option);
                $('#custom_proxy_sel').append(option);
            }

            if (check_flag == true && item['status'] != 3) {
                
                $.ajax({
                    "async": true,
                    "crossDomain": true,
                    "url": "https://gogetproxy.com/checker/check",
                    "method": "POST",
                    "headers": {
                        "Content-Type": "application/x-www-form-urlencoded",
                    },
                    "data": {
                        "proxies[]": item['proxy'] + ":" + item['port'] + ":" + item['user'] + ":" + item['pass']
                    }
                })
                .done(function (response) {
                    respJson = JSON.parse(response);
                    if (respJson.https == true || respJson.socks4 == true || respJson.socks5 == true) {
                        knex('proxy_list').where({ id: item['id']}).update({ status: 1 }).then(function(res){ 
                            $('#proxy-' + item['id'] + ' .status').removeClass('error');
                            $('#proxy-' + item['id'] + ' .status').removeClass('queued');
                            $('#proxy-' + item['id'] + ' .status').addClass('working');
                            $('#proxy-' + item['id'] + ' .status').text("Working");
                        });
                        
                    } else {
                        knex('proxy_list').where({ id: item['id']}).update({ status: 2 }).then(function(res){ 
                            $('#proxy-' + item['id'] + ' .status').removeClass('queued');
                            $('#proxy-' + item['id'] + ' .status').addClass('error');
                            $('#proxy-' + item['id'] + ' .status').text("Error");
                        });
                    }
                });
            }
        }
    });
    knex('userinfo').select().then(function(result) {
        user_data = result[0];
        $('.round').attr("avatar", user_data['username']);
        $('.license-key').html(user_data['auth_key']);
        $('.username').html(user_data['username'] + " (" + user_data['useremail'] + ")");

        LetterAvatar.transform();
    });
    
    knex.select().from('settings').first().then(function(data) {
        pushNotification = data['push_notify'];
        audioNotification = data['audio_notify'];
        if (data['push_notify'] == 1)
            $('.push-notification input').attr('checked', 'checked');
        else
            $('.push-notification input').removeAttr('checked');
        
        if (data['audio_notify'] == 1)
            $('.audio-notification input').attr('checked', 'checked');
        else
            $('.audio-notification input').removeAttr('checked');
        refresh_task_table();
    });
}

function generateRand(length) {
    let result           = '';
    let characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    let charactersLength = characters.length;

    for (let i = 0; i < length; i ++) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
 }

load_data(true);
