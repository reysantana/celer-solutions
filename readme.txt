[PROJECT SETUP]
1. Clone this repository to "celer-solutions"
2. Extract celer-solutions\node_modules.zip file in the project directory.

[RUN]
npm start

[BUILD]
1. electron-packager . 
2. Copy(Replace existing) node_modules folder to celer-solutions\celer-solutions-win32-x64\resources\app\node_modules
