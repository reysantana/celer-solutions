const { app, BrowserWindow, protocol } = require('electron');
const ipc = require('electron').ipcMain;
const path = require('path');
const { getAppDataPath } = require("appdata-path");
const knex = require("knex")({
	client: "sqlite3",
	connection: {
        filename: path.join(getAppDataPath('celer-solutions'), 'database.db'), 
  }, 
  useNullAsDefault: true
});
const axios = require('axios');
const https = require('https');
const qs = require('qs');
const notifier = require('node-notifier');
const fs = require('fs');

let authWindow = null;
let mainWindow = null;
let taskWindowsArray = {};

let isEmptyDatabase = false;

let proxyUsername = '';
let proxyPassword = '';

app.on('window-all-closed', function() {
  if (process.platform != 'darwin') {
    app.quit();
  }
});

app.on('login', function(event, webContents, request, authInfo, callback) { 
  if (authInfo.isProxy) {
     callback(proxyUsername, proxyPassword); 
  } 
});

app.on('ready', async function() {
  await knex.schema.hasTable('userinfo').then(function(exists) {
    if (!exists) {
      isEmptyDatabase = true;
      knex.schema
        .createTable('userinfo', function(table) {
            table.increments('id');
            table.string('auth_key');
            table.string('username');
            table.string('useremail');
        })
        .createTable('proxy_list', function(table) {
            table.increments('id');
            table.string('proxy');
            table.string('port');
            table.string('user');
            table.string('pass');
            table.integer('status');
        })
        .createTable('task_list', function(table) {
            table.increments('id');
            table.string('url');
            table.integer('status');
            table.integer('proxy_id').unsigned().references('proxy_list.id');
        })
        .createTable('settings', function(table) {
          table.increments('id');
          table.integer('push_notify');
          table.integer('audio_notify');
        })
        .then(function() {
          knex('proxy_list').insert({ proxy:'localhost', status : -1}).then(function() {});
          return knex('settings').insert({ push_notify: 1, audio_notify: 1 });
        });
    } else {
      isEmptyDatabase = false;

      knex.select().from("userinfo").first().then(function(data) {
        if (typeof data == "undefined" || data == null){
          showAuthWindow();
        } else {
          axios.post('https://celerqueueserver.com/index.php/API/login', qs.stringify({ license: data['auth_key'] })
          ).then(function (response) {
            if (response.data.name.length > 0 && response.data.email.length > 0 && response.data.message == "Success") {
              showMainWindow();
              closeAuthWindow();
            } else {
              showAuthWindow();
            }
          }).catch(function(response) {
            showAuthWindow();
          }).finally(function() {
          });
        }
      });
    }
  });

  ipc.on('closeTask', function(event, title) {
    closeTaskWindow(title);
  });

  // ipc.on('taskUrlChanged', function(event, title) {
  //   closeTaskWindow(title);
  // });

  ipc.on('queueTask', function(event, title, url, ip, port, user, pass) {    
    if (taskWindowsArray.hasOwnProperty(title)) {
      taskWindowsArray[title].loadURL(url);
    }
  });

  ipc.on('runTask', function(event, title, url, ip, port, user, pass) {    
    if (taskWindowsArray.hasOwnProperty(title) == false) {
      proxyUsername = user;
      proxyPassword = pass;
      createTaskWindow(title, url, ip, port, user, pass);
    }
  });

  ipc.on('minimizeTask', function() {
    for (let i in taskWindowsArray) {
      if (taskWindowsArray[i] != null)
      taskWindowsArray[i].minimize();
    }
  });

  ipc.on('mass_refresh', function() {
    for (let i in taskWindowsArray) {
      if (taskWindowsArray[i] != null)
      taskWindowsArray[i].reload();
    }
  });

  ipc.on('refreshTask', function(event, title) {
    if (taskWindowsArray.hasOwnProperty(title)) {
      taskWindowsArray[title].reload();
    }
  });
  ipc.on('maximizeTask', function(event, title) {
    if(title == null){
      for (let i in taskWindowsArray) {
        if (taskWindowsArray[i] != null)
        taskWindowsArray[i].show();
      }
    } else {
      if (taskWindowsArray.hasOwnProperty(title)) {
        taskWindowsArray[title].show();
      }
    }
  });
  
  ipc.on('auth_success', function(event, license, name, email, push_notify, audio_notify) {
    knex('userinfo').insert({ auth_key : license , username : name, useremail : email }).then(function(result) {
      showMainWindow();
      closeAuthWindow();
      showPushNotification(push_notify, audio_notify, `Thank you for logging in to Celer Queue App.`);
    });
  });

  ipc.on('logout', function(event, push_notify, audio_notify) {
    knex('userinfo')
    .where({ id: 1 })
    .del().then(function(result) {
      showAuthWindow();
      closeMainWindow();
      showPushNotification(push_notify, audio_notify, `You have been logged out from the Celer Queue App.`);
    });
  });

  ipc.on('deactivate', function(event, push_notify, audio_notify) {
    knex.destroy().then(function() {
      fs.unlink(path.join(getAppDataPath('celer-solutions'), 'database.db'), (err) => {
        if (err) {
          console.log(err);
          showPushNotification(push_notify, audio_notify, `Failed to deactivate your license key.`);
          return;
        }
        
        showPushNotification(push_notify, audio_notify, `You have deactivated your license key.`);
        showAuthWindow();
        closeMainWindow();
      });
    });
  });
  ipc.on('closeAllTasks', function() {
    for (let i in taskWindowsArray) {
      if (taskWindowsArray[i] != null)
        taskWindowsArray[i].close();
    }
  });
  ipc.on('close_auth', function() {
    closeAuthWindow();
  });

  let showMainWindow = function() {
    mainWindow = new BrowserWindow({
      width: 1200,
      height: 800,
      minWidth: 1200,
      minHeight: 800,
      darkTheme: true, 
      acceptFirstMouse: true,
      titleBarStyle: 'hidden',
      webPreferences: {
        nodeIntegration: true,
        nodeIntegrationInWorker: true, 
        devTools: false
      }
    });

    protocol.registerBufferProtocol('test', (request, callback) => {
      callback({mimeType: 'text/html', data: fs.readFileSync('file://' + __dirname + '/main.html')})
    }, (error) => {
      if (error) console.error('Failed to register protocol')
    })

    mainWindow.setMenuBarVisibility(false);
    mainWindow.loadURL('file://' + __dirname + '/main.html');
    mainWindow.webContents.on('did-finish-load', function() {
      mainWindow.webContents.send('mainWindowLoaded');
    });
    mainWindow.on('closed', function() {
      for (let i in taskWindowsArray) {
        if (taskWindowsArray[i] != null)
          taskWindowsArray[i].close();
      }
      mainWindow = null;
    });
  }

  let showAuthWindow = function() {
    authWindow = new BrowserWindow({
      width: 540,
      height: 270,
      minWidth: 540,
      minHeight: 270,
      maxWidth: 540,
      maxHeight: 270,
      darkTheme: true, 
      frame: false, 
      acceptFirstMouse: true,
      titleBarStyle: 'hidden',
      webPreferences: {
        nodeIntegration: true,
        nodeIntegrationInWorker: true, 
        devTools: false, 
      }
    });

    authWindow.setMenuBarVisibility(false);
    authWindow.loadURL('file://' + __dirname + '/auth.html');
    authWindow.on('closed', function() {
      authWindow = null
    });
  }

  let createTaskWindow = function(title, url, proxyIp, port, username, password) {
    let window = new BrowserWindow({
      width: 600,
      height: 400,
      minWidth: 600,
      minHeight: 400,
      acceptFirstMouse: true, 
      center: true, 
      title: title + "(Loading Proxy Settings...)", 
      darkTheme: true, 
      webPreferences: {
          nodeIntegration: true,
          nodeIntegrationInWorker: true,
          webviewTag: true, 
          devTools: true, 
          allowRunningInsecureContent: true, 
          plugins: true
      }
    });
    window.removeMenu();
    if (proxyIp == "localhost") {
      window.loadURL(url);
    } else {
      console.log("Configuring Proxy Settings");
      window.webContents.session.setProxy({proxyRules: "http://" + proxyIp + ":" + port}, function () {
        console.log("Finished Loading Proxy Settings");
        window.loadURL(url);
      });
    }
    window.webContents.on('did-finish-load', function() {
      window.setTitle(title + " (" + proxyIp + ")");
    });
    window.once("ready-to-show", () => {
      window.show();
      window.focus();
    });
    window.webContents.on('will-navigate', function(event, url) {
      knex.select().from('settings').first().then(function(data) {
        event.preventDefault();
        console.log("Passed: " + title + " " + url);
        mainWindow.webContents.send('taskUrlChanged', title);
        push_notify = data['push_notify'];
        audio_notify = data['audio_notify'];
        showPushNotification(push_notify, audio_notify, title + ` has been passed.`);
      });
    });
    window.on('closed', function() {
      if (mainWindow != null && mainWindow.webContents != null) {
        mainWindow.webContents.send('taskClosed', title);
      }
      delete taskWindowsArray[title];
      window = null;
    });
    taskWindowsArray[title] = window;
  }

  let closeMainWindow = function() {
    if (mainWindow != null)
      mainWindow.close();
  }

  let closeAuthWindow = function() {
    if (authWindow != null)
      authWindow.close();
  }

  let closeTaskWindow = function(title) {
    if (taskWindowsArray[title] != null) {
      taskWindowsArray[title].close();
    }
  }

  if (isEmptyDatabase == true) {
    showAuthWindow();
  }

  let showPushNotification = function(push_notify, audio_notify, message) {
    if (push_notify == 1) {
      notifier.notify({
        appId: 'com.celersolutions.app',
        title: 'Celer Queue',
        time: 5000, 
        message: message,
        sound: audio_notify == 0 ? false : true, 
        icon: path.join(__dirname, 'images/logo.png'),
        wait: true
      }, (err) => {
        if (err) {
          console.error('Cannot show notification. Error: ', err);
        }
      });
    }
  }
});